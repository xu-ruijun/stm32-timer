/*
 * my_timer.h
 *
 *  Created on: Mar 15, 2021
 *      Author: xrj
 */

#ifndef SRC_MY_LIBS_MY_TIMER_H_
#define SRC_MY_LIBS_MY_TIMER_H_

void STOP_sec(uint32_t sec);

#endif /* SRC_MY_LIBS_MY_TIMER_H_ */
